<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

        // ----------------- demo1
        // $x = 1;

        // 单引号与双引号的区别
        // 单引号输出的就是字符串
        // 双引号内部可以解析变量
        // echo 'Hello Word! $x <br>';
        // echo "Hello Word! $x";

        // ------------------ demo2
        // 直接输出变量进行相加需要加上()
        // $x = 5;
        // $y = 3;
        // echo '$x与$y相加的结果' .( $x + $y);

        // ------------------ demo3
        $x = 7;

        echo "<h1>在页面中显示变量 $x</h1>";
    ?>
</body>
</html>