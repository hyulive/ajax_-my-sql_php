let cont = $(".cont");

// 渲染数据
function render(data) {
    let html = "";
    if (data.code == 0) {
        // 判断是否成功移除数据
        if (data.flag) {
            loadAll();
            alert("成功移除数据！");
            return;
        }
        // 判断是否成功添加数据
        if (data.addFlag) {
            loadAll();
            alert("成功添加数据！");
            return;
        }
        data.list.forEach(item => {
            html += `
                <tr>
                    <td>${item.Id}</td>
                    <td>${item.title}</td>
                    <td><img src="https:${item.imgUrl}" alt=""></td>
                    <td>${item.hot}</td>
                    <td><button class='compile' data-id=${item.Id}>编辑</button></td>
                    <td><button class='del' data-id=${item.Id}>删除</button></td>
                </tr>
            `;
        })
    } else {
        if (data.flag || data.addFlag) {
            alert("你没有权限！");
            return;
        }
        html = `
            <tr>暂无数据</tr>
        `
    }
    cont.innerHTML = html;
}

// 向ajax发送请求
function ajaxRequest(url) {
    // 发起ajax请求
    var xhr = new XMLHttpRequest();
    // console.log(url);
    xhr.open("get", `./api/${url}`, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let data = JSON.parse(xhr.responseText);
            render(data);
            $(".mask").classList.remove("active");
            $(".add-pannel").classList.remove("active");
        }
    }
}

// 初始化页面加载数据
function loadAll() {
    ajaxRequest("selectAll.php");
}

// 搜索
function selectTitle() {
    let searchBtn = $(".toolbar .search-btn");
    let searchBox = $(".toolbar .search-box");

    searchBtn.onclick = function(e) {
        e.preventDefault();
        let titleVal = searchBox.value;
        ajaxRequest(`select_title.php?title=${titleVal}`);
    }
}

// 删除
function delData() {
    cont.onclick = function(e) {
        if (e.target.classList.contains("del")) {
            let id = e.target.dataset.id;
            ajaxRequest(`del.php?id=${id}`);
        }
    }
}

// 添加数据
function addData() {
    var hideBtn = $(".toolbar .add");
    var addBtn = $(".add-pannel .add-btn");
    var title = $(".add-pannel .title");
    var imgUrl = $(".add-pannel .imgUrl");
    var hotCheck = $(".add-pannel .hot");

    hideBtn.onclick = function() {
        $(".mask").classList.add("active");
        $(".add-pannel").classList.add("active");
    }
    addBtn.onclick = function(e) {
        e.preventDefault();
        let titleVal = title.value;
        let imgUrlVal = imgUrl.value;
        let hot = hotCheck.checked ? 1 : 0;
        ajaxRequest(`add.php?title=${titleVal}&imgUrl=${imgUrlVal}&hot=${hot}`);
    }

}

// 关闭添加按钮弹出弹出框
function closeBtn() {
    var btn = $(".close");

    btn.onclick = function() {
        $(".mask").classList.remove("active");
        $(".add-pannel").classList.remove("active");
    }
}
loadAll();
selectTitle();
delData();
addData();
closeBtn();