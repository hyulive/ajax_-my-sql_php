let page = 1;
let flag = true;

// 渲染热销商品数据
function renderHot(data) {
    let html = "";
    if (data.code == 0) {
        data.list.forEach(item => {
            html += `
            <li>
                <p>Id: ${item.Id}</p>
                <p>标题: ${item.title}</p>
                <p><img src="https:${item.imgUrl}" alt=""></p>
            </li>
            `
        })
    }
    $(".hot ul").innerHTML = html;
}

// 初始化加载热销商品
function loadHot() {
    let url = './api/loadHotData.php';
    ajax(url, renderHot);
}

// 渲染猜你喜欢数据
function renderFav(data) {
    let html = "";
    if (data.code == 0) {
        data.list.forEach(item => {
            html += `
            <li>
                <p>Id: ${item.Id}</p>
                <p>标题: ${item.title}</p>
                <p><img src="https:${item.imgUrl}" alt=""></p>
            </li>
            `
        })
    }
    $(".more ul").innerHTML += html;
    flag = true;
}

// 初始化加载猜你喜欢的数据数据
function loadFavData() {
    let url = `./api/loadFavData.php? page=${page}`;
    ajax(url, renderFav);
}

// 触底加载数据
function scrollLoad() {
    let winHeight = window.innerHeight;
    window.onscroll = function() {
        let docHeight = document.documentElement.getBoundingClientRect().height;
        let scrollTop = document.documentElement.scrollTop;

        if (!flag) {
            return;
        }

        if (scrollTop + winHeight >= docHeight - 100) {
            flag = false;
            page++;
            let url = `./api/loadFavData.php? page=${page}`;
            ajax(url, renderFav);
        }
    }
}

loadHot();
loadFavData();
scrollLoad();