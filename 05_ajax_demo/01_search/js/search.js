// 渲染数据
function render(data) {
    let cont = $(".cont");
    let html = '';
    if (data.code == 0) {
        data.list.forEach(item => {
            html += `
            <tr>
                <td>${item.Id}</td>
                <td>${item.title}</td>
                <td>${item.hot}</td>
                <td>图片缩略图</td>
                <td><button data-id=${item.Id} class="edit">编辑</button></td>
                <td><button data-id=${item.Id} class="del">删除</button></td>
            </tr>
            `
        })
    } else {
        html = `
        <tr>
            暂无数据
        </tr>
        `
    }
    cont.innerHTML = html;
}

function loadData() {
    let url = './api/selectAll.php';
    ajax(url, render);
}

function searchFrh() {
    let search = $(".search");
    let timer;
    search.oninput = function() {
        // clearTimeout(timer);
        // timer = setTimeout(() => {
        //     // console.log(this.value);
        //     let keyword = this.value;
        //     let url = `./api/searchFrh.php?keyword=${keyword}`;
        //     ajax(url, render);
        // }, 300);
        let xhr = new XMLHttpRequest();
        xhr.abort();

        let keyword = this.value;
        let url = `./api/searchFrh.php?keyword=${keyword}`;
        ajax(url, render);
    }
}

loadData();
searchFrh();