//获取dom
function $(selector) {
    return document.querySelector(selector)
}

function ajax(url, callback) {
    let xhr = new XMLHttpRequest()
    xhr.open("get", url, true)
    xhr.send()
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log(JSON.parse(xhr.responseText));
            let data = JSON.parse(xhr.responseText);
            callback(data);
        }
    }
}