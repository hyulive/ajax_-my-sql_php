// 获取dom函数
function $(selector) {
    var domList = document.querySelectorAll(selector);
    if (domList.length == 0) {
        return null;
    }
    if (domList.length == 1) {
        return domList[0];
    }
    return domList;
}

// 随机数
function rnd(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

// 向ajax发送请求
function ajaxRequest(url, callback) {
    // 发起ajax请求
    var xhr = new XMLHttpRequest();
    // console.log(url);
    xhr.open("get", `./api/${url}`, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let data = JSON.parse(xhr.responseText);
            callback(data);
        }
    }
}